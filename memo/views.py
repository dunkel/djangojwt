# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.http import HttpResponse
from django.contrib.auth.models import User
from django.contrib.auth.decorators import user_passes_test
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from rest_framework_jwt.settings import api_settings
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from django.template import RequestContext

def email_check(user):
    return user.is_authenticated

def index(request):
    return HttpResponse("Hello, world. You're at the polls index.")

@api_view(['GET','POST'])
@permission_classes((IsAuthenticated, ))
def hello_world(request):
    if request.method == 'POST':
        return Response({"message": "Got some data!", "data": request.data})
    return Response({"message": "Hello, world!"})

@user_passes_test(email_check, login_url='/admin/login/')
def prueba(request):
    #return HttpResponse("Hello, world. You're at the polls index.")
    #return render_to_response('memo/template/prueba.html')
    return render(request, 'prueba.html', {
        'foo': 'bar',
    })
@api_view(['GET','POST'])
@permission_classes((IsAuthenticated, ))
def helloworld(request):
    if request.method == 'POST':
        return Response({"message": "Got some data!", "data": request.data})
    return Response({"message": "Hello, world!"})
class ExampleView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request, format=None):
        content = {
            'status': 'request was permitted'
        }
        return Response(content)