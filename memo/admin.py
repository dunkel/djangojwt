# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

# Register your models here.
from .models import UrlMapping

#@admin.register(UrlMapping)
class UrlMappingAdmin(admin.ModelAdmin):
    list_display = ['id_url_mapping', 'request_path', 'target_path', 'http_code', 'created_at','updated_at']
    list_filter = ('id_url_mapping','request_path',)
    ordering = ('-created_at',)
admin.site.register(UrlMapping,UrlMappingAdmin)
