import json
from django.db import IntegrityError,transaction
from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth.models import User


class Command(BaseCommand):
    def __init__(self, *args, **kwargs):
        """
        :param args: 
        :param kwargs: 
        """
        super(Command, self).__init__(*args, **kwargs)

    def add_arguments(self, parser):
        """
        :param parser: 
        :return: 
        """
        parser.add_argument('request', nargs='+', type=str)

    def handle(self, *args, **options):
        """
        :param args: 
        :param options: 
        :return: 
        """
        'python manage.py edit_user name password'
        'python manage.py edit_user "john2" "jlennon@beatles.com"'
        try:
            user = User.objects.create_user(username=options['request'][0], email=options['request'][1],
                                          password=options['request'][2])
            result = {'code': 200, 'msg': 'Successfully closed poll "%s"' % options['request']}
            self.stdout.write(self.style.SUCCESS(json.dumps(result)))
        except IntegrityError as err:
            result = {'code': 202, 'msg': err.message}
            self.stdout.write(self.style.ERROR(json.dumps(result)))
        except Exception as err:
            result = {'code': 202, 'msg': err.message}
        except CommandError as err:
            result = {'code': 202, 'msg': err.message}
            self.stdout.write(self.style.ERROR(json.dumps(result)))