from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth.models import User

class Command(BaseCommand):
    def __init__(self, *args, **kwargs):
        """
        :param args: 
        :param kwargs: 
        """
        super(Command, self).__init__(*args, **kwargs)

    def add_arguments(self, parser):
        """
        :param parser: 
        :return: 
        """
        parser.add_argument('poll_id', nargs='+', type=str)

    def handle(self, *args, **options):
        """
        :param args: 
        :param options: 
        :return: 
        """
        'python manage.py my_command "john" "jlennon@beatles.com" "glass onion"'
        user = User.objects.create_user(username=options['poll_id'][0],email =options['poll_id'][1],password =options['poll_id'][2])

        self.stdout.write(self.style.SUCCESS('Successfully closed poll "%s"' % options['poll_id']))